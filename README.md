# Programación de Vídeojuegos


Este es un repositorio donde subiré mis prácticas del módulo de Programación de Vídejuegos, también se subirán algunos proyectos en lenguaje C orientado a objetos que aunque no son precisamente vídeojuegos también son parte de las prácticas del módulo.


Puedes contactarme en la siguiente dirección de correo electrónico:
![](http://i.imgur.com/0Alhs5g.png)


[Licencia del Repositorio](https://gitlab.com/eduardormz/programacion-videojuegos/blob/master/LICENSE)