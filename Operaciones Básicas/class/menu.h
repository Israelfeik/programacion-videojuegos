#include <conio.h>
#include <stdio.h>

class menu{
	private:
	int op;
	
	public:
	menu();
	~menu();
	void solicitarOpcion(void);
	int obtenerOpcion(void);
};

menu::menu(){
	op = 0;
}

menu::~menu(){
}

void menu::solicitarOpcion(void){
	printf("\n 1. Suma \n 2. Resta \n 3. Division \n 4. Multiplicacion");
	printf("\n Selecciona una opcion (1, 2, 3, 4): ");
	scanf("%i", &op);
}

int menu::obtenerOpcion(void){
	return op;
}
